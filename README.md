# TestEmployeeClasses-in-Java



public class TestEmployeeClass {

public static void main(String[] args){
 
 HourlyEmployee clerk = new HourlyEmployee("Sam", "1234", "clerk", "", "", 30, 2000, 15000.00, 40.0, 15.0);
 SalaryEmployee supervisor = new SalaryEmployee("Jessica", "2222", "supervisor", "", "", 33, 1998, 4000.00, 52000.00);
 
 NewEmployee spouseOfBoss = new NewEmployee("George", "3456");
 
 double weekpay = clerk.calcWeeklyPay();
 clerk.updateTotalPay(weekPay);
 System.out.println(clerk.toString());
 System.out.println(clerk.getName() + "  " + clerk.getSocial() + ", weekly salary $" + weekPay + "\n");
 weekPay = supervisor.calcWeeklyPay();
 supervisor.updateTotalPay(weekPay);
 System.out.println(supervisor.toString());
 System.out.println(supervisor.getName() + " " + supervisor.getSocial() + ", weekly salary $" + weekPay + "\n");
 spouseOfBoss.updateTotalPay(500.00);
 System.out.println(spouseOfBoss.toString() + "\n");
 if (clerk.equals(supervisor))
 System.out.println(clerk.getName() + " and " + supervisor.getName() + " are same employee");
 
  else
   System.out.println(clerk.getName() + " and " + supervisor.getName() + " are different employee");
 }
}  
 
